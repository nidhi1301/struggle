CREATE TABLE IF NOT EXISTS kalavathiuser  (
  user_id SERIAL unique NOT NULL PRIMARY KEY ,
  username varchar(45) UNIQUE NOT NULL,
  email varchar(45),
  user_password varchar(45)
);
