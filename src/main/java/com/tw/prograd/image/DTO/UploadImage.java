package com.tw.prograd.image.DTO;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class UploadImage {
    private Integer id;
    private String name;
}
