package com.tw.prograd.image.contoller;

import com.tw.prograd.image.DTO.UploadImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<KalavithiUsers> view() {
        return userRepository.findAll();
    }

    public void add(KalavithiUsers user) {
        System.out.println("*****************service");
        System.out.println(user);
        System.out.println("*****************service");
        userRepository.save(user);
        System.out.println("*****************userSaved");
    }
}
