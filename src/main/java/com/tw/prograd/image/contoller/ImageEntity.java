package com.tw.prograd.image.contoller;

import com.tw.prograd.image.DTO.UploadImage;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "image")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer image_id;

    @Column(nullable = false)
    private String image_name;

    public UploadImage toSavedImageDTO() {
        return new UploadImage(image_id, image_name);
    }
}
