package com.tw.prograd.image.contoller;

import com.tw.prograd.image.DTO.UploadImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.net.URI;
import java.util.List;

import static org.springframework.http.HttpStatus.FOUND;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(value = "kalavithi/users")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<KalavithiUsers> getUsersList() {
        return userService.view();
    }

    @PostMapping
    public void createUser(@RequestBody KalavithiUsers user) {
        System.out.println("*****************");
        System.out.println(user);
        System.out.println(user.getUsername());
        System.out.println("*****************");
        userService.add(user);
    }

}
