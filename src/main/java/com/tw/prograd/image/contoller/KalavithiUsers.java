package com.tw.prograd.image.contoller;

import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name="kalavithiusers")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class KalavithiUsers {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long user_id;

    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String user_password;
}
